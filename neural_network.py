import numpy
import scipy.special
import matplotlib.pyplot

# одношарова штучна нейронна мережа
class NeuralNetwork:

    # ініціалізація нейронної мережі
    def __init__(self,
                 input_nodes_number,
                 hidden_nodes_number,
                 output_nodes_number,
                 training_factor):

        self.input_neurons_number = input_nodes_number
        self.hidden_neurons_number = hidden_nodes_number
        self.output_neurons_number = output_nodes_number

        self.weights_matrix_IH = numpy.random.normal(
            0.0, pow(self.hidden_neurons_number, -0.5),
            (self.hidden_neurons_number, self.input_neurons_number))

        self.weights_matrix_HO = numpy.random.normal(
            0.0, pow(self.output_neurons_number, -0.5),
            (self.output_neurons_number, self.hidden_neurons_number))

        self.learning_factor = training_factor

        self.activation_function = lambda x: scipy.special.expit(x)

        pass

    # тренування нейронної мережі
    def train(self, input_signals, correct_output_signals):
        input_values_matrix = numpy.array(input_signals, ndmin=2).T
        correct_values_matrix = numpy.array(correct_output_signals, ndmin=2).T

        hidden_inputs = numpy.dot(self.weights_matrix_IH, input_values_matrix)
        hidden_outputs = self.activation_function(hidden_inputs)

        final_inputs = numpy.dot(self.weights_matrix_HO, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)

        output_errors = correct_values_matrix - final_outputs
        hidden_errors = numpy.dot(self.weights_matrix_HO.T, output_errors)

        self.weights_matrix_HO += self.learning_factor * \
                                  numpy.dot(
                                      (output_errors * final_outputs * (1.0 - final_outputs)),
                                      numpy.transpose(hidden_outputs)
                                  )

        self.weights_matrix_IH += self.learning_factor * \
                                  numpy.dot(
                                      (hidden_errors * hidden_outputs * (1.0 - hidden_outputs)),
                                      numpy.transpose(input_values_matrix)
                                  )
        pass

    # запит нейронної мережі
    def query(self, input_signals):
        input_values_matrix = numpy.array(input_signals, ndmin=2).T

        hidden_inputs = numpy.dot(self.weights_matrix_IH, input_values_matrix)
        hidden_outputs = self.activation_function(hidden_inputs)

        final_inputs = numpy.dot(self.weights_matrix_HO, hidden_outputs)
        final_output_signals = self.activation_function(final_inputs)

        return final_output_signals


# Тренування нейронної мережі

input_nodes_number = 784
hidden_nodes_number = 100
output_nodes_number = 10
training_factor = 0.3

neural_network = NeuralNetwork(input_nodes_number,
                  hidden_nodes_number,
                  output_nodes_number,
                  training_factor)

training_data_file = open("mnist_dataset/mnist_train_60000_full.csv", 'r')
training_data_list = training_data_file.readlines()
training_data_file.close()

# Демонстрація зображення з набору MNIST
# image_array = numpy.asfarray(training_data_list[0].split(',')[1:]).reshape((28, 28))
# matplotlib.pyplot.imshow(image_array, cmap='Greys', interpolation='None')
# matplotlib.pyplot.show()

for line in training_data_list:
    all_values = line.split(',')
    input_signals = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
    correct_output_signals = numpy.zeros(output_nodes_number) + 0.01
    correct_output_signals[int(all_values[0])] = 0.99
    neural_network.train(input_signals, correct_output_signals)
    pass


# Тестування нейронної мережі

test_data_file = open("mnist_dataset/mnist_test_10000_full.csv", 'r')
test_data_list = test_data_file.readlines()
test_data_file.close()

scores = []
for record in test_data_list:
    all_values = record.split(',')
    correct_digit = int(all_values[0])
    print(correct_digit, "справжнє значення")
    input_signals = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
    output_signals = neural_network.query(input_signals)
    output_digit = numpy.argmax(output_signals)
    print(output_digit, "відповідь нейронної мережі")
    if output_digit == correct_digit:
        scores.append(1)
    else:
        scores.append(0)
        pass

    pass

print(scores)
scores_array = numpy.asarray(scores)
print("ефективність = ", scores_array.sum() / scores_array.size)